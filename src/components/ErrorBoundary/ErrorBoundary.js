import React, {Component} from 'react';

class ErrorBoundary extends Component {
  state = {
    hasError: false,
    errorMessage: null,
    error: null
  };

  componentDidCatch(error, info) {
    this.setState({hasError: true, errorMessage: info, error: error});
    console.log(this.state.errorMessage);
  };

  render () {
    if (this.state.hasError) {
      return <div>Something wrong happened</div>;
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
