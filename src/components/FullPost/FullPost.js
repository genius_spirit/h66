import React, { Component} from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import './FullPost.css';

class FullPost extends Component {

  state = {
    loadedPost: []
  };

  getPost = (id) =>{
    axios.get(`/post/${id}.json`).then(response => {
      this.setState({loadedPost: response.data});
    });
  };

  deletePostHandler = (id) => {
    axios.delete(`/post/${id}.json`).then(() => {
      this.props.history.push('/posts');
    });
  };

  componentDidMount() {
    this.getPost(this.props.match.params.id);
  }

  render() {
    if (this.state.loadedPost) {
      return(
        <div className="FullPost">
          <div className="post-time">{this.state.loadedPost.time}</div>
          <h3>{this.state.loadedPost.title}</h3>
          <div dangerouslySetInnerHTML={{__html: this.state.loadedPost.post}}/>
          <p>
            <Link to={"/posts/" + this.props.match.params.id + "/edit"}
                  className="FullPost-btn-edit">Edit post</Link>
            <input type="submit" value="Delete post" className="FullPost-btn-delete"
                   onClick={() => this.deletePostHandler(this.props.match.params.id)} />
            <Link to={'/posts'} className="FullPost-btn-back">Back</Link>
          </p>
        </div>
      )
    }
  }
}

export default FullPost;