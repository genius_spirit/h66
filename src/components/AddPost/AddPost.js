import React, {Component} from 'react';
import './AddPost.css';
import axios from 'axios';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

class AddPost extends Component {

  state = {
    title: '',
    post: '',
  };

  changeValueHandler = (event) => {
    this.setState({[event.target.name]: event.target.value})
  };

  handleChange = (value) => {
    this.setState({ post: value });
  };


  addPostHandler = (event) => {
    event.preventDefault();
    let post = {title: this.state.title, post: this.state.post, time: new Date()};
    axios.post('/post.json', post).then(() => {
      this.props.history.push('/posts');
    })
  };

  editPostHandler = (event) => {
    event.preventDefault();
    const id = this.props.match.params.id;
    axios.patch(`/post/${id}.json`, this.state).then(() => {
      this.props.history.push('/');
    })
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      const id = this.props.match.params.id;
      axios.get(`/post/${id}.json`).then(r => {
        this.setState({title: r.data.title, post: r.data.post});
      });
    }
  }

  render() {
    return (
      <div>
        <form className='AddPost-form'>
          { this.props.match.path === '/posts/add'
            ? <h3>Add new post form:</h3>
            : <h3>Edit post form</h3> }
          <input type="text" name="title" value={this.state.title} onChange={(event) => this.changeValueHandler(event)}/>
          <ReactQuill value={this.state.post} onChange={this.handleChange} />
          {this.props.match.path === '/posts/add'
            ? <button className="AddPost-btn" onClick={this.addPostHandler}>Save</button>
            : <button className="AddPost-btn" onClick={this.editPostHandler}>Edit</button> }
        </form>
      </div>
    )
  }

}

export default AddPost;